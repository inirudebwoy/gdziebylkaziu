# -*- coding: utf-8 -*-
import os

from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = \
    patterns('',
             (r'^admin/', include(admin.site.urls)),
             (r'^/?$', 'gdzie_byl_kaziu.gjm.views.index'),
             (r'^pobierzdane/$',
              'gdzie_byl_kaziu.gjm.views.panel_info'),

             # dajaxice
             (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX,
              include('dajaxice.urls')),
        )

if settings.MODE in ('LOCAL', 'DEVEL'):
    urlpatterns += \
        patterns('',
                 (r'^templates/static/(?P<path>.*)$',
                  'django.views.static.serve',
                  {'document_root':
                   os.path.join(os.path.dirname(__file__),
                                ('/home/michal/Projects/gdzie_byl_kaziu/'
                                 'templates/static'))}))
