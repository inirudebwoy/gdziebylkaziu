var marker;
var markers = [];
var infoWindow;
var infoWindows = [];
var contentString;
var map;
var geocoder;
var adres;
var iw_content_szukaj = '<div id="przyciski"><button id="zbliz_mape" type="button">Zbliż</button><button type="button" id="pokaz_dodaj">Dodaj</button>';
var iw_content_dodaj;
var direction_display;

$(function() {
    //all hover and click logic for buttons
    $(".fg-button:not(.ui-state-disabled)")
	.hover(
	    function(){ 
		$(this).addClass("ui-state-hover"); 
	    },
	    function(){ 
		$(this).removeClass("ui-state-hover"); 
	    }
	)
	.mousedown(function(){
	    $(this).parents('.fg-buttonset-single:first').find(".fg-button.ui-state-active").removeClass("ui-state-active");
	    if( $(this).is('.ui-state-active.fg-button-toggleable, .fg-buttonset-multi .ui-state-active') ){ $(this).removeClass("ui-state-active"); }
	    else { $(this).addClass("ui-state-active"); }	
	})
	.mouseup(function(){
	    if(! $(this).is('.fg-button-toggleable, .fg-buttonset-single .fg-button,  .fg-buttonset-multi .fg-button') ){
		$(this).removeClass("ui-state-active");
	    }
	});
    // error.js
    pola = new Array();
    bledy = new Array();

    $("div[id='hide']").children().each(function() {
	pola.push($(this).attr("name"));
	bledy.push($(this).html());
    });
    $("input").each(function() {
	index = jQuery.inArray($(this).attr("name"), pola);
	if (index != -1)
	{
	    $(this).attr("title", bledy[index]).tooltip();	    
	}
    });

    // all.js
    $("a").each(function() {
	if ($(this).attr("href") == window.location.pathname + window.location.search)
	{
	    $(this).addClass("current");
	    return false;
	}
    });
    
    // statystyki
    $("button[id='uzytkownicy']").click(function() {
	pobierz_staty('uzytkownicy.ilosc.5', 'graph-lewy');
	pobierz_staty('uzytkownicy.wartosc.5', 'graph-prawy');
    });
    $("button[id='banknoty']").click(function() {
	pobierz_staty('banknoty.ilosc.0', 'graph-lewy');
	pobierz_staty('banknoty.wartosc.0', 'graph-prawy');
    });

    // index.js

    $('#wyszukiwanie_message').hide();

    $('#wpisz_adres').keypress(function(event) {
	if (event.keyCode == 13)
	{
	    szukaj_adres();
	}
    });

    $('#nowy_banknot').hide();

    $('#nowy_banknot').click(function() {
	var banknot = 'ost';
	Dajaxice.gjm.pobierz_dane('Dajax.process', {'banknot': banknot});
	$(this).hide();
	return false;
    });

    var cia = JSON.parse($.cookie('gdzie_byl_kaziu'));
	if (cia)
	{
	    var ile_lok = cia['ost'].length;
	    var ost_lok = cia['ost'][ile_lok - 1][0].split(',');
	    var myLatlng = new google.maps.LatLng(ost_lok[0], ost_lok[1]);
	}
	else
	{
	    var myLatlng = new google.maps.LatLng('54','20');
	}
    
    if (document.location.pathname == '/')
    {	
	var myOptions = {
	    zoom: 6,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    mapTypeControl: false,
	    navigationControl: true,
	    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}
	}
	direction_display = new google.maps.DirectionsRenderer();
	map = new google.maps.Map(document.getElementById("mapa"),
				  myOptions);
	direction_display.setMap(map);	
	google.maps.event.addListener(map,
				      'center_changed',
				      function(event) {
					  $('#wyszukiwanie_message').hide();
				      });	
	google.maps.event.addListener(map, 'click', function(event) {
	    if (marker != null)
	    {
		marker.setMap(null);
	    }
	    if (infoWindow != null)
	    {
		infoWindow.close();
	    }
	    marker = new google.maps.Marker({
		position: event.latLng, 
		map: map
	    });
	    
	    if(contentString == '' || contentString === undefined)
	    {
		contentString = document.getElementById('a-form');
	    }
	    var wspolrzedne = event.latLng;
	    contentString.elements['id_szer_geogr'].value = wspolrzedne.lat();
	    contentString.elements['id_dl_geogr'].value = wspolrzedne.lng();
	    
	    infoWindow = new google.maps.InfoWindow({
		content: contentString
	    });
	    google.maps.event.addListener(infoWindow,
					  'closeclick',
					  function(event) {
		marker.setMap(null);
	    });
	    infoWindow.open(map, marker);
	});
	var banknot = 'ost';
	Dajaxice.gjm.pobierz_dane('Dajax.process', {'banknot': banknot});
	
	$("div[id='ajax_results']") //załaduj dane na starcie
	    .load('/pobierzdane/');
	
	$(document).everyTime(10000, function() {
	    $("div[id='ajax_results']")	    
		.load('/pobierzdane/'); // aktualizacja danych
	}, 0);
	
	$.ajax({
	    type: "POST",
	    url: "/accounts/auth_check",
	    error: function(req, text, err){
		if (req.status == 403)
		{
		    $("#messages").html("<p>Nie jesteś zalogowany. Nie będzie możliwe śledzenie dodanych banknotów.<button type='button'>Zamknij</button></p>");
		}
	    }
	});
    }
    
    if (document.location.pathname.search('statystyki/uzytkownicy') != -1)
    {
	var myOptions = {
	    zoom: 6,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    mapTypeControl: false,
	    navigationControl: true,
	    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}
	}
	
	map = new google.maps.Map(document.getElementById("mapa"),
				  myOptions);
    }

    $("#messages :button").live("click", function() {
	$(this).parent().remove();
    });

    // wyłączamy inputy
    $('input[name="dl_geogr"]').attr('readonly','readonly');
    $('input[name="szer_geogr"]').attr('readonly','readonly');

    //dajaxice wysyła formularz
    $('#a-form-button').click(function(event) {
	send_form();	
	return false;
    });

    $('#szukaj_adres').click(function(event) {
	szukaj_adres();
    });

    // przyciski iw_content_szukaj
    $('#zbliz_mape').live('click', function(event) {
	map.setZoom(12);
    });

    $('#pokaz_dodaj').live('click', function(event) {
	if(contentString == '' || contentString === undefined)
	{
	    contentString = document.getElementById('a-form');
	}
	var wspolrzedne = marker.getPosition();
	contentString.elements['id_szer_geogr'].value = wspolrzedne.lat();
	contentString.elements['id_dl_geogr'].value = wspolrzedne.lng();
	$('#szukaj_info').html(contentString);
    });

    $('#box-table-a a').live('click', function(event) {
	var banknot = $(this).attr('id');
	var trasa = true;	
	Dajaxice.gjm.pobierz_dane('Dajax.process', {'banknot': banknot,
						    'trasa': trasa});
	$('#nowy_banknot').show();
	//czysc_markers();
	//czysc_infowindows();
	return false;
    });

    // ładuj dane do selecta
    var ciacho;
    try
    {
	ciacho = $.cookie('gdzie_byl_kaziu');
	laduj_select(ciacho);
    }
    catch(err)
    {
	ciacho = '<option>Brak lokalizacji</option>';	
    }

    // event od wyszukiwania nowych wartości
    $('#ostat_lokal').change(function() {	
	var id_mark = $(this).find(' :selected').attr('id');
	if (id_mark != '')
	{
	    var lat_lng = id_mark.split(',');
	    if (marker != null)
	    {
		marker.setMap(null);
	    }
	    if (infoWindow != null)
	    {
		infoWindow.close();
	    }

	    var l = new google.maps.LatLng(lat_lng[0], lat_lng[1]);
	    map.setCenter(l);
	    marker = new google.maps.Marker({
		position: l,
		map: map
	    });
	    
	    if(contentString == '' || contentString === undefined)
	    {
		contentString = document.getElementById('a-form');
	    }
	    contentString.elements['id_szer_geogr'].value = l.lat();
	    contentString.elements['id_dl_geogr'].value = l.lng();
	    
	    infoWindow = new google.maps.InfoWindow({
		content: contentString
	    });
	    google.maps.event.addListener(infoWindow,
					  'closeclick', 
					  function(event) {
		marker.setMap(null);
	    });
	    infoWindow.open(map, marker);
	}
    }); 
});

function placeMarker(location, map)
{
  map.setCenter(location);
}

function send_form()
{
    var data = $('#a-form').serializeObject();
    var c = $.cookie('gdzie_byl_kaziu');
    Dajaxice.gjm.weryfikuj_dane('Dajax.process', {'form': data,
						  'ciacho': c});
}

function clear_map()
{
    var markers_length = markers.length;
    for (var i=0, len=markers_length; i < len; ++i)
    {
	markers[i].setMap(null);
    }
}

function szukaj_adres()
{
    $('#wyszukiwanie_message').hide().html('');
    var ar_len;
    var region = 'pl';
    adres = $('#wpisz_adres').val();
    geocoder = new google.maps.Geocoder();
    if (geocoder)
    {
	geocoder.geocode({'address': adres, 'region': region}, function(results, status) {
	    var ar_len = results.length;
	    if (status == google.maps.GeocoderStatus.OK)
	    {
		//clear_map();
		map.setCenter(results[0].geometry.location);
		map.setZoom(9);

		for (var i=0, len=ar_len; i < len; ++i) {
		var f_address = results[i].formatted_address;
		    marker = new google.maps.Marker({
			map: map,
			position: results[i].geometry.location,
			title: f_address
		    });
		    google.maps.event.addListener(marker, 'click', function(event) {			
			iw_content = '<div id="szukaj_info">' + f_address + iw_content_szukaj + '</div>';
			infoWindow = new google.maps.InfoWindow({
			    content: iw_content
			});
			infoWindow.open(map, this);
			google.maps.event.addListener(infoWindow, 'closeclick', function(event) {
			    marker.setMap(null);
			});
		    });
		}
	    }
	    else
	    {
		if (status == 'ZERO_RESULTS')
		{		    
		    $('#wyszukiwanie_message').show()
			.html('Ciemność, widzę ciemność.');
		}
	    }
	});
    }
    else
    {
	$('#wyszukiwanie_message').html('Geocoder nie działa.');
    }
}

function prepare_map(data)
{
    var j_object = JSON.parse(data);
    var ar_len = j_object.length;

    for (var i=0, len=ar_len; i < len; ++i) {
	// współrzędne
	var wspolrzedne = new google.maps.LatLng(j_object[i]['fields']['szer_geogr'],j_object[i]['fields']['dl_geogr']);

	// nowy marker
	marker = new google.maps.Marker({
	    position: wspolrzedne,
	    map: map
	});
	
	// wyłapiemy click!
	google.maps.event.addListener(marker, 'click', function(event) {
	    infoWindow = new google.maps.InfoWindow({
		content: 'Dupsko' // dane pobierane z tablicy?
	    });
	    infoWindow.open(map, this);
	});
    }
}

function rysuj_trase(dane)
{
    if (dane)
    {
	var d = JSON.parse(dane);
	var d_len = d.length;
	var typ_podrozy = google.maps.DirectionsTravelMode.DRIVING;
	var directionsService = new google.maps.DirectionsService();

	clear_map();
	czysc_markers();
	czysc_infowindows();
	$('#nowy_banknot').show();
	
	for (var i=0, len=d_len; i < len; ++i)
	{
	    var wspolrzedne = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
	    var i_window = '<p>Data dodania: ' +
		d[i]['fields']['data_dodania'] + '</p>' +
		'<p>Przez: ' + d[i]['fields']['login_uzytkownika'] + 
		'</p>' +
		'<p>Dł. geogr: ' + d[i]['fields']['dl_geogr'] + '</p>' +
		'<p>Sz. geogr: ' + d[i]['fields']['szer_geogr'] + '</p>' +
		'<p>Notka: ' + d[i]['fields']['notka'] + '</p>';
	    var marker = new google.maps.Marker({		 
			map: map,
			position: wspolrzedne		
	    });	    
	    google.maps.event.addListener(marker, 'click', function(event) {
		// szukamy markera i dodajemy odp. infoWindow
		for (var i=0, len=d_len; i < len; ++i)
		{
		    if (markers[i].position == this.position)
		    {
			infoWindow = new google.maps.InfoWindow({
			    content: infoWindows[i]
			});
		    }
		}
		infoWindow.open(map, this);
	    });
	    markers.push(marker);
	    infoWindows.push(i_window);
	}

	if (d_len == 1) // nie ma drogi, jeden punkt
	{
	    var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
	    marker = new google.maps.Marker({
		position: start,
		map: map
	    });
	    map.setCenter(marker.position);
	}
	else if (d_len == 2) // dwa punkty
	{
	    var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
	    var koniec = new google.maps.LatLng(d[1]['fields']['szer_geogr'], d[1]['fields']['dl_geogr']);
	    var request = {
		origin: start,
		destination: koniec,
		travelMode: typ_podrozy		
	    };
	}
	else // wiele, ale mniej niż 8
	{
	    var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
	    var koniec = new google.maps.LatLng(d[d_len-1]['fields']['szer_geogr'], d[d_len-1]['fields']['dl_geogr']);
	    var przystanki = new Array();
	    for (var i=1, len=d_len-1; i < len; ++i)
	    {
		var punkt = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
		var przystanek = {location: punkt,
				  stopover: true}
		przystanki.push(przystanek);
	    }
	    var request = {
		origin: start,
		destination: koniec,
		travelMode: typ_podrozy,
		waypoints: przystanki
	    };   
	}
	if (request)
	{
	    directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK)
		{
		    direction_display.setDirections(result);
		}
	    });
	}
    }
}

function rysuj_punkty(dane)
{
    if (dane)
    {
	var d = JSON.parse(dane);
	var d_len = d.length;
	
	for (var i=0, len=d_len; i < len; ++i)
	{
	    var wspolrzedne = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
	    var i_window = '<p>Data dodania: ' + d[i]['fields']['data_dodania'] + '</p>' +
		'<p>Przez: ' + d[i]['fields']['login_uzytkownika'] + '</p>' +
		'<p>Dł. geogr: ' + d[i]['fields']['dl_geogr'] + '</p>' +
		'<p>Sz. geogr: ' + d[i]['fields']['szer_geogr'] + '</p>' +
		'<p>Notka: ' + d[i]['fields']['notka'] + '</p>';

//	    var marker_icon = new google.maps.MarkerImage({
//		url: '../templates/static/gfx/marker_ff9900.png'
//	    });
	    
	    var marker = new google.maps.Marker({		 
		map: map,
		position: wspolrzedne
//		icon: marker_icon
	    });	    
	    google.maps.event.addListener(marker, 'click', function(event) {
		// szukamy markera i dodajemy odp. infoWindow
		for (var i=0, len=d_len; i < len; ++i)
		{
		    if (markers[i].position == this.position)
		    {
			infoWindow = new google.maps.InfoWindow({
			    content: infoWindows[i]
			});
		    }
		}
		infoWindow.open(map, this);
	    });
	    markers.push(marker);
	    infoWindows.push(i_window);
	}
    }
    else
    {
	alert('Nic do wyświetlenia');
    }
}

function rysuj_tabele(dane)
{
    $('#table_container').html(dane);
}

function alert(txt)
{
    alert(txt);
}

function zachowaj_ciastko(ciacho)
{
    $.cookie('gdzie_byl_kaziu', ciacho, {expires: 100});
    laduj_select(ciacho);
    return false;
}

function rysuj_wykres(dane)
{
    var d = JSON.parse(dane);
    var sel_cel = '#' + d['cel'];    
    $(sel_cel).tufteBar({
	data: d['dane'],
	axisLabel: function(index) {return this[1].label},
	barLabel:  function(index) {
	    if (d['rodzaj'] == 'ilosc')
	    {
		return $.tufteBar.formatNumber(this[0]) + 'x';
	    }
	    else if (d['rodzaj'] == 'wartosc')
	    {
		return parseInt(this[0]) + ' zł';
	    }
	},
	barWidth: 0.75,
	color:     function(index) { 
	    return ['#ff9900', '#3299bb'][index % 2] 
	}
    });
    $(sel_cel).parent().find('h3').html(d['naglowek'][0]);
    $(sel_cel).parent().find('p').html(d['naglowek'][1]);
}

function laduj_select(dane)
{
    if (dane == null)
    {
	select_html = '<option>Brak lokalizacji</option>';
    }
    else
    {
	var select_html = '<option>Wybierz</option>';
	$.each(JSON.parse(dane)['ost'], function(index, value) {
	    select_html += ('<option id="' + value[0]  + '">' 
			    + value[1] + '</option>');
	});
    }
    $('#ostat_lokal').html(select_html);
}

function czysc_markers()
{
    markers = [];
    if (marker != null)
    {
	marker.setMap(null);
    }
}

function czysc_infowindows()
{
    infoWindows = [];
    if (infoWindow != null)
    {
	infoWindow.close();
    }
}

function pobierz_staty(jakie, gdzie)
{
    Dajaxice.gjm.statystyki('Dajax.process', {'co' : jakie,
					      'cel' : gdzie});
}