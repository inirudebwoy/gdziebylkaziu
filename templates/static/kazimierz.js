function szukaj_adres()
{
    $('#wyszukiwanie_message').hide().html('');
    var ar_len;
    var region = 'pl';
    var adres = $('#wpisz_adres').val();
    var geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({'address': adres, 'region': region},
                         function(results, status) {
                             var ar_len = results.length;
                             if (status == google.maps.GeocoderStatus.OK)
                             {
                                 kaziu.map.setCenter(results[0].geometry.location);
                                 kaziu.map.setZoom(9);

                                 for (var i=0, len=ar_len; i < len; ++i) {
                                     var f_address = results[i].formatted_address;
                                     kaziu.marker = new google.maps.Marker({
                                         map: kaziu.map,
                                         position: results[i].geometry.location,
                                         title: f_address
                                     });
                                     google.maps.event.addListener(kaziu.marker, 'click', function(event) {
                                         var iw_content_szukaj = '<div id="przyciski"><button id="zbliz_mape" type="button">Zbliż</button><button type="button" id="pokaz_dodaj">Dodaj</button>';
                                         iw_content = '<div id="szukaj_info">' + f_address + iw_content_szukaj + '</div>';
                                         kaziu.infoWindow = new google.maps.InfoWindow({
                                             content: iw_content
                                         });
                                         kaziu.infoWindow.open(kaziu.map, this);
                                         google.maps.event.addListener(kaziu.infoWindow, 'closeclick', function(event) {
                                             kaziu.marker.setMap(null);
                                         });
                                     });
                                 }
                             } else {
                                 if (status == 'ZERO_RESULTS')
                                 {
                                     $('#wyszukiwanie_message').show()
                                         .html('Ciemność, widzę ciemność.');
                                 }
                             }
                         });
    } else {
        $('#wyszukiwanie_message').html('Geocoder nie działa.');
    }
}

function rysuj_trase(dane)
{
    if (dane) {
        var d = JSON.parse(dane);
        var d_len = d.length;
        var typ_podrozy = google.maps.DirectionsTravelMode.DRIVING;
        var directionsService = new google.maps.DirectionsService();

        kaziu.clear_map();
        kaziu.clear_markers();
        kaziu.clear_infowindows();
        $('#nowy_banknot').show();

        for (var i=0, len=d_len; i < len; ++i)
        {
            var wspolrzedne = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
            var i_window = '<p>Data dodania: ' +
                d[i]['fields']['data_dodania'] + '</p>' +
                '<p>Przez: ' + d[i]['fields']['login_uzytkownika'] +
                '</p>' +
                '<p>Dł. geogr: ' + d[i]['fields']['dl_geogr'] + '</p>' +
                '<p>Sz. geogr: ' + d[i]['fields']['szer_geogr'] + '</p>' +
                '<p>Notka: ' + d[i]['fields']['notka'] + '</p>';
            var marker = new google.maps.Marker({
                map: kaziu.map,
                position: wspolrzedne
            });
            google.maps.event.addListener(marker, 'click', function(event) {
                // szukamy markera i dodajemy odp. infoWindow
                for (var i=0, len=d_len; i < len; ++i)
                {
                    if (kaziu.markers[i].position == this.position)
                    {
                        kaziu.infoWindow = new google.maps.InfoWindow({
                            content: kaziu.infoWindows[i]
                        });
                    }
                }
                kaziu.infoWindow.open(kaziu.map, this);
            });
            kaziu.markers.push(marker);
            kaziu.infoWindows.push(i_window);
        }

        if (d_len == 1) {// nie ma drogi, jeden punkt
            var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
            kaziu.marker = new google.maps.Marker({
                position: start,
                map: kaziu.map
            });
            kaziu.map.setCenter(kaziu.marker.position);
        } else if (d_len == 2) {// dwa punkty
            var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
            var koniec = new google.maps.LatLng(d[1]['fields']['szer_geogr'], d[1]['fields']['dl_geogr']);
            var request = {
                origin: start,
                destination: koniec,
                travelMode: typ_podrozy
            };
        } else { // wiele, ale mniej niż 8
            var start = new google.maps.LatLng(d[0]['fields']['szer_geogr'], d[0]['fields']['dl_geogr']);
            var koniec = new google.maps.LatLng(d[d_len-1]['fields']['szer_geogr'], d[d_len-1]['fields']['dl_geogr']);
            var przystanki = new Array();
            for (var i=1, len=d_len-1; i < len; ++i)
            {
                var punkt = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
                var przystanek = {location: punkt,
                                  stopover: true}
                przystanki.push(przystanek);
            }
            var request = {
                origin: start,
                destination: koniec,
                travelMode: typ_podrozy,
                waypoints: przystanki
            };
        }
        if (request) {
            directionsService.route(request, function(result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    kaziu.direction_display.setDirections(result);
                }
            });
        }
    }
}

function rysuj_punkty(dane)
{
    if (dane) {
        var d = JSON.parse(dane);
        var d_len = d.length;

        for (var i=0, len=d_len; i < len; ++i)
        {
            var wspolrzedne = new google.maps.LatLng(d[i]['fields']['szer_geogr'], d[i]['fields']['dl_geogr']);
            var i_window = '<p>Data dodania: ' + d[i]['fields']['data_dodania'] + '</p>' +
                '<p>Przez: ' + d[i]['fields']['login_uzytkownika'] + '</p>' +
                '<p>Dł. geogr: ' + d[i]['fields']['dl_geogr'] + '</p>' +
                '<p>Sz. geogr: ' + d[i]['fields']['szer_geogr'] + '</p>' +
                '<p>Notka: ' + d[i]['fields']['notka'] + '</p>';

            var marker = new google.maps.Marker({
                map: kaziu.map,
                position: wspolrzedne
            });
            google.maps.event.addListener(marker, 'click', function(event) {
                // szukamy markera i dodajemy odp. infoWindow
                for (var i=0, len=d_len; i < len; ++i)
                {
                    if (kaziu.markers[i].position == this.position)
                    {
                        kaziu.infoWindow = new google.maps.InfoWindow({
                            content: kaziu.infoWindows[i]
                        });
                    }
                }
                kaziu.infoWindow.open(kaziu.map, this);
            });
            kaziu.markers.push(marker);
            kaziu.infoWindows.push(i_window);
        }
    } else {
        alert('Nic do wyświetlenia');
    }
}
