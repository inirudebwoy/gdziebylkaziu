kaziu = {
    marker: null,
    markers_list: [],
    infoWindow: null,
    infoWindows_list: [],
    content_string: null,
    map: null,
    direction_display: null
    errors: [],
    fields: [],
    clear_map: () ->
        marker.setMap null for marker in kaziu.markers_list
        true
    clear_markers: () ->
        this.markers_array = []
        this.marker?.setMap null
        true
    clear_infowindows: () ->
        this.infoWindows_list = []
        this.infoWindow?.close()
        true
    }

$ ->
    add_hover = (object) ->
        object.addClass "ui-state-hover"
    rm_hover = (object) ->
        object.removeClass "ui-state-hover"
    mousedown_active = (object) ->
        $(this).parents('.fg-buttonset-single:first').
            find(".fg-button.ui-state-active").
            removeClass("ui-state-active")
        if $(this).is('.ui-state-active.fg-button-toggleable, .fg-buttonset-multi .ui-state-active')
            $(this).removeClass "ui-state-active"
        else
            $(this).addClass "ui-state-active"
    mouseup_active = (object) ->
        if !$(this).is('.fg-button-toggleable, .fg-buttonset-single .fg-button, .fg-buttonset-multi .fg-button')
            object.removeClass "ui-state-active"

    $(".fg-button:not(.ui-state-disabled)").hover add_hover, rm_hover
    $(".fg-button:not(.ui-state-disabled)").mousedown mousedown_active
    $(".fg-button:not(.ui-state-disabled)").mouseup mouseup_active

    $("div[id='hide']").children().each () ->
        kaziu.fields.push $(this).attr "name"
        kaziu.errors.push $(this).html
        true

    $("input").each () ->
        index = jQuery.inArray $(this).attr("name"), kaziu.fields
        if index != -1
            $(this).attr("title", kaziu.errors[index]).tooltip()
        true

    $("a").each () ->
        if $(this).attr "href" == window.location.pathname + window.location.search
            $(this).addClass "current"
            false

    $('#wyszukiwanie_message').hide()
    $('#nowy_banknot').hide()
    $('#nowy_banknot').click () ->
        Dajaxice.gjm.pobierz_dane 'Dajax.process', {'banknot': 'ost'}
        $(this).hide()
        false

    $('#wpisz_adres').keypress (event) ->
        if event.keyCode == 13
            szukaj_adres()
            true

    $('#szukaj_adres').click (event) ->
        szukaj_adres()
        true

    $("#messages :button").live "click", () ->
        $(this).parent().remove()
        true

    # wyłączamy inputy
    $('input[name="dl_geogr"]').attr 'readonly','readonly'
    $('input[name="szer_geogr"]').attr 'readonly','readonly'

    # dajaxice wysyła formularz
    $('#a-form-button').click (event) ->
        send_form(this)
        false

    # przyciski iw_content_szukaj
    $('#zbliz_mape').live 'click', (event) ->
        map.setZoom(12)
        true

    $('#pokaz_dodaj').live 'click', (event) ->
        if contentString == '' || contentString == undefined
            contentString = document.getElementById 'a-form'
        wspolrzedne = kaziu.marker.getPosition()
        contentString.elements.id_szer_geogr.value = wspolrzedne.lat()
        contentString.elements.id_dl_geogr.value = wspolrzedne.lng()
        $('#szukaj_info').html contentString

    $('#box-table-a a').live 'click', (event) ->
        banknot = $(this).attr 'id'
        Dajaxice.gjm.pobierz_dane 'Dajax.process', {'trasa': true, 'banknot': banknot}
        $('#nowy_banknot').show()
        false

    # ładuj dane do selecta
    try
        ciacho = $.cookie 'gdzie_byl_kaziu'
        fill_select ciacho
    catch err
        ciacho = '<option>Brak lokalizacji</option>'

    # event od wyszukiwania nowych wartości
    $('#ostat_lokal').change () ->
        id_mark = $(this).find(' :selected').attr 'id'
        if id_mark != ''
            if kaziu.marker?
                kaziu.marker.setMap null
            if infoWindow?
                infoWindow.close()

            [lat, lng] = id_mark.split ','
            location = new google.maps.LatLng lat, lng
            map.setCenter location
            kaziu.marker = create_marker location, map
            add_marker_info kaziu.marker, map
            true

    cia = JSON.parse $.cookie 'gdzie_byl_kaziu'
    if cia?
        [last_lat, last_lng] = cia['ost'][cia.ost.length - 1][0].split ','
        myLatlng = new google.maps.LatLng last_lat, last_lng
    else
        myLatlng = new google.maps.LatLng '54','20'

    if document.location.pathname == '/'
        myOptions =
            zoom:
                6
            center:
                myLatlng
            mapTypeId:
                google.maps.MapTypeId.ROADMAP
            mapTypeControl:
                false
            navigationControl:
                true
            navigationControlOptions:
                style:
                    google.maps.NavigationControlStyle.SMALL

        direction_display = new google.maps.DirectionsRenderer()
        map = new google.maps.Map document.getElementById("mapa"), myOptions
        direction_display.setMap map
        google.maps.event.addListener map, 'center_changed', (event) ->
            $('#wyszukiwanie_message').hide()

        google.maps.event.addListener map, 'click', (event) ->
            if $('#howto').size() > 0
                $('#howto').remove()
            if kaziu.marker?
                kaziu.marker.setMap null
            if infoWindow?
                infoWindow.close()
            kaziu.marker = create_marker event.latLng, map
            add_marker_info kaziu.marker, map

        Dajaxice.gjm.pobierz_dane 'Dajax.process', {'banknot': 'ost'}
        load_stats()
        $(document).everyTime(500000, load_stats, 0)

        auth_check_args =
            type:
                "POST"
            url:
                "/accounts/auth_check"
            error:
               (req, text, err) ->
                   if req.status == 403
                       $("#messages").html "<p>Nie jesteś zalogowany. Nie będzie możliwe śledzenie dodanych banknotów.<button type='button'>Zamknij</button></p>"
        $.ajax auth_check_args

    if document.location.pathname.search('statystyki/uzytkownicy') != -1
        myOptions = {
            zoom: 6,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}}
        map = new google.maps.Map document.getElementById("mapa"), myOptions
        # sprawdzać document.location.search i na podstawie tego
        # budować listę lokalizacji do wyświetlenia
        # wszystko w ajax.py i ładowane przez dajax.
        Dajaxice.gjm.banknoty_uzytkownika 'Dajax.process', {'czesc': document.location.search}
    true

prepare_map = (data) ->
    data = JSON.parse data
    new_point = (location) ->
        lat_lng = new google.maps.LatLng location.fields.szer_geogr, location.fields.dl_geogr
        # nowy kaziu.marker
        kaziu.marker = create_marker  lat_lng, map
        # wyłapiemy click!
        google.maps.event.addListener kaziu.marker, 'click', (event) ->
            infoWindow = new google.maps.InfoWindow {content: 'Dupsko'}
            infoWindow.open map, this
    new_point location for location in data

create_marker = (lat_lng, map) ->
    new google.maps.Marker {position: lat_lng, map: map}

add_marker_info = (marker, map) ->
    marker_latlng = marker.getPosition()
    if contentString == '' || contentString == undefined
        contentString = document.getElementById 'a-form'
    contentString.elements.id_szer_geogr.value = marker_latlng.lat()
    contentString.elements.id_dl_geogr.value = marker_latlng.lng()
    infoWindow = new google.maps.InfoWindow {content: contentString}
    google.maps.event.addListener infoWindow, 'closeclick', (event) ->
        kaziu.marker.setMap null
        true
    infoWindow.open map, kaziu.marker
    true

load_stats = () ->
    $("div[id='ajax_results']").load '/pobierzdane/'

fill_select = (data) ->
    if data?
        select_html = '<option>Wybierz</option>'
        select_html += "<option id='#{value[0]}'>#{value[1]}</option>" for value in JSON.parse(data['ost'])
    else
        select_html = '<option>Brak lokalizacji</option>'
    $('#ostat_lokal').html select_html
    true

save_cookie = (cookie) ->
    $.cookie 'gdzie_byl_kaziu', cookie, {expires: 100}
    fill_select ciacho
    true

display_jgrowl = (text) ->
    $.jGrowl text['wiadomosc'], {header: text['naglowek'], theme: 'mgrowl', life: 5000}
    true

fill_table = (html) ->
    $('#table_container').html data
    true

send_form = (form) ->
    Dajaxice.gjm.weryfikuj_dane 'Dajax.process', {'form': form.serializeObject(), 'ciacho': $.cookie('gdzie_byl_kaziu')}
    true

draw_chart = (data) ->
    data = JSON.parse data
    target_id = '#' + data['cel']
    chart_settings = {
        data: data['dane']
        axisLabel: (index) ->
            this[1].label
        barWidth: 0.75
        barLabel: (index) ->
            if d['rodzaj']
                "#{$.tufteBar.formatNumber(this[0])} x"
            else
                "#{parseInt(this[0])} zł"
        color: (index) ->
            ['#ff9900', '#3299bb'][index % 2]
    }
    $(target_id).tufteBar chart_settings
    $(target_id).parent().find('h3').html(data['naglowek'][0])
    $(target_id).parent().find('p').html(data['naglowek'][1])
    true
