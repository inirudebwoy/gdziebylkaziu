# -*- coding: utf-8 -*-
# Django settings for gdzie-byl-kaziu project.
import os

# where am i?
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# where are you?
# LOCAL, DEVEL, PROD
MODE = 'LOCAL'

# dajaxice settings
DAJAXICE_MEDIA_PREFIX = "dajaxice"
DAJAXICE_FUNCTIONS = (
    'gjm.ajax.weryfikuj_dane',
    'gjm.ajax.pobierz_dane',
)

ADMINS = (
    ('Michal Klich', 'michal@michalklich.com'),
)
MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Warsaw'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'pl'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'f)bd3$$#6ctp96wh8dh!4_&k(s%gd7gkq-tho*42r!t=21=w__'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader')

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
    #    'debug_toolbar.middleware.DebugToolbarMiddleware'
)
#INTERNAL_IPS = ('127.0.0.1',)
#DEBUG_TOOLBAR_PANELS = (
#    'debug_toolbar.panels.version.VersionDebugPanel',
#    'debug_toolbar.panels.timer.TimerDebugPanel',
#    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
#    'debug_toolbar.panels.headers.HeaderDebugPanel',
#    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
#    'debug_toolbar.panels.template.TemplateDebugPanel',
#    'debug_toolbar.panels.sql.SQLDebugPanel',
#    'debug_toolbar.panels.signals.SignalDebugPanel',
#    'debug_toolbar.panels.logger.LoggingPanel',
#)
ROOT_URLCONF = 'gdzie_byl_kaziu.urls'
TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
)
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'gdzie_byl_kaziu.gjm',
    'django.contrib.messages',
    'dajaxice',
    'dajax',
    #'debug_toolbar',
    #'django_extensions' #tylko dla devel
)

# Dodatkowe pola do modelu uzytkownika
AUTH_PROFILE_MODULE = "gjm.UserExtended"

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages")

#MESSAGE_STORAGE = "django.contrib.messages.storage.fallback.FallbackStorage"
ACCOUNT_ACTIVATION_DAYS = 7
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

if MODE == 'LOCAL':
    try:
        from settings_local import *
    except:
        pass
elif MODE == 'DEVEL':
    try:
        from settings_devel import *
    except:
        pass
else:
    try:
        from settings_prod import *
    except:
        pass
