# -*- coding: utf-8 -*-
from django import forms


ER_INVALID_SN_TXT = ("Niepoprawny numer seryjny. Sprawdź czy numer składa "
                     "się z 2 liter i 7 cyfr Np: XY1234567")


class DodajBanknot(forms.Form):
    """
    TODO

    """
    invalid_SN_txt = ("Niepoprawny numer seryjny. Sprawdź czy numer składa "
                      "się z 2 liter i 7 cyfr Np: XY1234567")
    numer_seryjny = \
        forms.RegexField('[A-Za-z]{2}\d{7}',
                         help_text="Numer składa się z 2 liter i 7 cyfr.",
                         error_messages={"invalid": ER_INVALID_SN_TXT})

    nominal = forms.ChoiceField(choices=[('10', '10'),
                                         ('20', '20'),
                                         ('50', '50'),
                                         ('100', '100'),
                                         ('200', '200')])

    kod_pocztowy = \
        forms.RegexField('\d{2}-\d{3}',
                         help_text=("Kod pocztowy oddzielony znakiem '-'. "
                                    "Na przykład: 41-200, 00-950 itp. "),
                         label='Kod pocztowy',
                         error_messages={'invalid':
                                         'Poprawna wartość to np: 41-200'})

    notka = \
        forms.CharField(label="Notka",
                        help_text=("Jeśli masz ciekawą historię dotyczącą "
                                   "banknotu to tu jest jej miejsce."),
                        required=False,
                        widget=forms.Textarea(attrs={'rows': 2, 'cols': 17}))

    szer_geogr = forms.CharField(widget=forms.HiddenInput, required=False)
    dl_geogr = forms.CharField(widget=forms.HiddenInput, required=False)


class powiadom(forms.Form):
    """
    TODO

    """
    powiadomienie = \
        forms.CharField(label="Informacje dotyczące kodu pocztowego",
                        help_text=("Wpisz informację pomagającą szybciej "
                                   "zweryfikować kod pocztowy."),
                        widget=forms.Textarea(attrs={'rows': 3, 'cols': 5}),
                        required=False)


class DodajBanknotWspl(forms.Form):
    """
    TODO

    """
    numer_seryjny = \
        forms.RegexField('[A-Za-z]{2}\d{7}',
                         help_text="Numer składa się z 2 liter i 7 cyfr.",
                         error_messages={"invalid": ER_INVALID_SN_TXT})

    nominal = forms.ChoiceField(choices=[('10', u'10 zł'),
                                         ('20', u'20 zł'),
                                         ('50', u'50 zł'),
                                         ('100', u'100 zł'),
                                         ('200', u'200 zł')])

    notka = \
        forms.CharField(label="Notka",
                        help_text=("Jeśli masz ciekawą historię dotyczącą "
                                   "banknotu to tu jest jej miejsce."),
                        required=False,
                        widget=forms.Textarea(attrs={'rows': 2, 'cols': 17}))
    szer_geogr = forms.CharField(label="Szerokość geogr.")
    dl_geogr = forms.CharField(label="Długość geogr.")
