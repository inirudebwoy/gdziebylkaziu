#-*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def multiply(value, arg):
    """
    Mnożenie wartości, do wyliczania wartości banknotów
    w tabelach. Bez sprawdzania danych więc lepiej uważać
    """
    return value * arg


@register.filter
def scrambler(value):
    """
    maskowanie numer seryjnego przez zastąpienie 5
    znaków kropkami. Zamiast kodu lepiej chyba filtr
    do templatu

    """
    return '%s.....%s' % (value[:2], value[7:])
