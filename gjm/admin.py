# -*- coding: utf-8 -*-
from django.contrib import admin
from gdzie_byl_kaziu.gjm.models import Banknot, Lokalizacja

admin.site.register(Banknot)
admin.site.register(Lokalizacja)
