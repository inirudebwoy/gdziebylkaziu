# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.db.models import Count, Sum

from forms import DodajBanknotWspl
from gdzie_byl_kaziu.gjm.models import Banknot, Lokalizacja


def index(request):
    user = request.user
    if request.method == 'POST':
        form_maps = DodajBanknotWspl(request.POST)
        if form_maps.is_valid():
            sn = request.POST['numer_seryjny'].upper()
            banknot, created = \
                Banknot.objects.get_or_create(numer_seryjny__exact=sn)

            l = Lokalizacja()
            l.login_uzytkownika = user
            l.banknot = banknot
            l.notka = form_maps.cleaned_data['notka']
            l.szer_geogr = form_maps.cleaned_data['szer_geogr']
            l.dl_geogr = form_maps.cleaned_data['dl_geogr']
            l.save()
        else:
            form_maps = DodajBanknotWspl()
    else:
        form_maps = DodajBanknotWspl()

    last_5_loc = Lokalizacja.objects.order_by('-data_dodania')[:5]
    return render_to_response('index.html',
                              {'form_maps': form_maps,
                               'user': user,
                               'ost_5': last_5_loc},
                              context_instance=RequestContext(request))


def panel_info(request):
    if request.is_ajax():
        bill_count = Banknot.objects.values('nominal_banknotu').\
            annotate(Count('nominal_banknotu'))
        value_total = 0
        for bill in bill_count:
            value_total += bill['nominal_banknotu'] * \
                bill['nominal_banknotu__count']

        km_total = Lokalizacja.objects.aggregate(Sum('km'))['km__sum'] or 0
        users_total = User.objects.all().count()

        return render_to_response('panel.html',
                                  {'users_total': users_total,
                                   'value_total': value_total,
                                   'km_total': km_total},
                                  context_instance=RequestContext(request))
