#-*- coding: utf-8 -*-
from django.db import models


class Banknot(models.Model):
    """
    TODO

    """
    numer_seryjny = models.CharField(max_length=9)
    nominal_banknotu = models.IntegerField()
    nr_hash = models.CharField(max_length=40,
                               blank=True)

    def __unicode__(self):
        """
        TODO

        """
        return self.numer_seryjny


class Lokalizacja(models.Model):
    """
    TODO

    """
    banknot = models.ForeignKey(Banknot)
    data_dodania = models.DateTimeField(auto_now_add=True)
    kod_pocztowy = models.CharField(max_length=6,
                                    blank=True)
    login_uzytkownika = models.CharField(max_length=30)
    notka = models.TextField(blank=True)
    km = models.IntegerField(default=0)
    dl_geogr = models.CharField(max_length=50,
                                default=0)
    szer_geogr = models.CharField(max_length=50,
                                  default=0)
    predkosc = models.FloatField(blank=True,
                                 editable=False,
                                 default=0)
    delta_punkt = models.FloatField(blank=True,
                                    editable=False,
                                    default=0)

    def __unicode__(self):
        """
        TODO

        """
        return u'Dł: %s, szer: %s' % (self.dl_geogr, self.szer_geogr)

    def _get_date_delta(self):
        """
        Returns time difference between two dates.

        """
        l = Lokalizacja.objects.filter(banknot=self.banknot).\
            filter(id__lte=self.id).order_by('id').reverse()[:2]
        if l.count() is None or l.count() == 1:
            return 0
        else:
            td = l[0].data_dodania - l[1].data_dodania
            return td

    def _get_coord(self):
        """
        TODO

        """
        try:
            szer_polkula = 'S'
            dl_polkula = 'W'
            if (self.szer_geogr > 0):
                szer_polkula = 'N'
            if (self.dl_geogr > 0):
                dl_polkula = 'E'
            dd_szer = self.szer_geogr.split('.')
            dd_dl = self.dl_geogr.split('.')
            min_szer = float('0.' + dd_szer[1]) * 60
            min_dl = float('0.' + dd_dl[1]) * 60
            sek_szer = (min_szer % 1) * 60
            sek_dl = (min_dl % 1) * 60

            return u'%s° %d\' %d\'\' %s, %s° %d\' %d\'\' %s' % \
                (dd_szer[0], min_szer, sek_szer, szer_polkula,
                 dd_dl[0], min_dl, sek_dl, dl_polkula)
        except:
            return False

    # rejestrowanie property dla modelu Lokalizacja
    delta = property(_get_date_delta)
    wspolrzedne = property(_get_coord)
