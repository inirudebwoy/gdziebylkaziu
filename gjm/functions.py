# -*- coding: utf-8 -*-
from django.db import connection


def formatTimeDelta(td):
    sec = td.days * 24 * 60 * 60 + td.seconds
    min, sec = divmod(sec, 60)
    hrs, min = divmod(min, 60)
    return '%02d:%02d:%02d' % (hrs, min, sec)


def delta_to_hours(td):
    return td.days * 24 + (float(td.seconds) / 3600)


def ukryj_numer_seryjny(nr):
    return '%s.....%s' % (nr[:2], nr[7:])


def sprawdz_kod(kod):
    """
    Sprawdzam czy kod istnieje w bazie

    @returns: boolean, True (exists) False (not exists)

    """
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM kody_new WHERE kod = %s", [kod])
    row = cursor.fetchall()
    if row:
        wynik = True
    else:
        wynik = False
    return wynik


def table_html_output(dane, format, nr_ser=False, nominal=False):
    """
    @dane      - queryset z danymi do wyświetlenia
    @format    - 'ost', 'banknot', 'trasa' w zależności od wybranej opcji
                      wyświetlany jest odpowiedni zestaw kolumn.
    @nr_ser    - nr seryjny banknotu, nieaktywne dla @format = 'ost'
    @nominal   - nominał banknotu, nieaktywne dla @format = 'ost'

    TODO
    wydaje mi się, że 'trasa' i 'banknot' mogą zostać połączone w jeden
    """
    html = ''

    if format == 'ost':
        html += '<div class="table_header_div"><p>' \
                'Ostatnie nabytki</p></div>' \
                '<table id="box-table-a"><thead><tr>' \
                '<th>Numer</th>' \
                u'<th>Nominał</th>' \
                '<th>Ostatnia aktualizacja</th>' \
                '<th>Gdzie</th>' \
                '<th>Kto</th>' \
                '</tr></thead>' \
                '<tbody>'
        for l in dane:
            wysw = {'numer': ukryj_numer_seryjny(l.banknot.numer_seryjny),
                    'nr_hash': l.banknot.nr_hash,
                    'nominal': l.banknot.nominal_banknotu,
                    'data_dodania': l.data_dodania,
                    'gdzie': l.wspolrzedne,
                    'kto': l.login_uzytkownika}
            html += '<tr>' \
                '<td><a href="#" id="%(nr_hash)s">%(numer)s</a></td>' \
                u'<td>%(nominal)s zł</td>' \
                '<td>%(data_dodania)s</td>' \
                '<td>%(gdzie)s</td>' \
                '<td>%(kto)s</td></tr>' % wysw
        html += '</table>'
    if format == 'banknot':
        html += '<div class="table_header_div"><p>' \
            'Banknot: %s' \
            u' Nominał: %s zł</p></div>' \
            '<table id="box-table-a"><thead><tr>' \
            '<th>Ostatnia aktualizacja</th>' \
            '<th>Gdzie</th>' \
            '<th>Kto</th>' \
            u'<th>Odległość</th>' \
            u'<th>Prędkość</th>' \
            '</tr></thead>' \
            '<tbody>' % (nr_ser,
                         nominal)
        for l in dane:
                wartosci_tabeli = {'data_dodania': l.data_dodania,
                                   'gdzie': l.wspolrzedne,
                                   'kto': l.login_uzytkownika,
                                   'odleglosc': l.km,
                                   'predkosc': l.predkosc}
                html += '<tr><td>%(data_dodania)s</td>' \
                    '<td>%(gdzie)s</td>' \
                    '<td>%(kto)s</td>' \
                    '<td>%(odleglosc)s km</td>' \
                    '<td>%(predkosc)s km/h</td></tr>' % wartosci_tabeli
        html += '</tbody>'
    if format == 'trasa':
        html += '<div class="table_header_div"><p>' \
            'Banknot: %s' \
            u' Nominał: %s zł</p></div>' \
            '<table id="box-table-a"><thead><tr>' \
            '<th>Ostatnia aktualizacja</th>' \
            '<th>Gdzie</th>' \
            u'<th>Odległość</th>' \
            u'<th>Prędkość</th>' \
            '</tr></thead>' \
            '<tbody>' % (nr_ser,
                         nominal)
        for l in dane:
            wartosci_tabeli = {'data_dodania': l.data_dodania,
                               'gdzie': l.wspolrzedne,
                               'odleglosc': l.km,
                               'predkosc': l.predkosc}
            html += '<tr><td>%(data_dodania)s</td>' \
                    '<td>%(gdzie)s</td>' \
                    '<td>%(odleglosc)s km</td>' \
                    '<td>%(predkosc)s km/h</td></tr>' % wartosci_tabeli
        html += '</tbody>'
    return html
