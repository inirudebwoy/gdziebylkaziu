# -*- coding: utf-8 -*-
import hashlib
import json

from django.core import serializers
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from forms import DodajBanknotWspl
from geopy import distance
from geopy.point import Point
from dajax.core import Dajax

from gdzie_byl_kaziu.gjm.functions import delta_to_hours
from gdzie_byl_kaziu.gjm.models import Banknot, Lokalizacja
from gdzie_byl_kaziu.gjm.functions import ukryj_numer_seryjny
from gdzie_byl_kaziu.gjm.functions import table_html_output


GROWL_TXT = {'powt_miejsce': {'wiadomosc': ('Twoja ostatnia lokalizacja '
                                            'dla tego banknotu powtarza się. '
                                            'Sory Gregory. Nie dodałem.'),
                              'naglowek': 'Błąd'}}


def pobierz_dane(request, banknot='ost', ilosc=7, trasa=False):
    """
    Funkcja zwracająca zadaną ilość ostatnich lokalizacji poprzez
    parametry
    - banknot = 'ost'
    - ilosc = ilosc banknotów

    Zwraca również zadaną ilość Lokalizacji dla wybranego banknotu
    - banknot = HASH
    - ilosc = ilosc
    """
    if request.is_ajax():
        dajax = Dajax()
        tabela = ''

        if banknot == 'ost':
            ost = Lokalizacja.objects.order_by('-data_dodania')[:ilosc]
            data = serializers.serialize('json', ost)
            dajax.add_data(data, 'rysuj_punkty')
            tabela = table_html_output(ost, 'ost')
            dajax.add_data(tabela, 'fill_table')
        elif banknot:
            b = get_object_or_404(Banknot, nr_hash=banknot)
            lista = Lokalizacja.objects.filter(banknot__exact=b.id).\
                order_by('-data_dodania')
            data = serializers.serialize('json',
                                         lista)
            if trasa:
                dajax.add_data(data, 'rysuj_trase')
            tab = table_html_output(lista,
                                    'banknot',
                                    ukryj_numer_seryjny(b.numer_seryjny),
                                    b.nominal_banknotu)
            dajax.add_data(tab, 'fill_table')

        dajax.script('clear_map()')
        return dajax.json()
    else:
        return False


def weryfikuj_dane(request, form, ciacho):
    dajax = Dajax()
    form = DodajBanknotWspl(form)
    user = request.user
    try:
        ciacho = json.loads(ciacho)
    except:
        ciacho = {'ost': []}
    ciacho_ok = True

    if form.is_valid():
        dajax.remove_css_class('#a-form input', 'error')

        try:
            sn = form.cleaned_data['numer_seryjny'].upper()
            b = Banknot.objects.get(numer_seryjny__exact=sn)
        except:
            b = Banknot()
            b.numer_seryjny = form.cleaned_data['numer_seryjny'].upper()
            b.nominal_banknotu = form.cleaned_data['nominal']
            b.nr_hash = hashlib.sha1(b.numer_seryjny).hexdigest()
            b.save()

        l = Lokalizacja()
        l.login_uzytkownika = user
        l.banknot = b
        l.notka = form.cleaned_data['notka']
        l.szer_geogr = form.cleaned_data['szer_geogr']
        l.dl_geogr = form.cleaned_data['dl_geogr']

        try:  # liczymy dystans
            ost_lok = Lokalizacja.objects.filter(banknot=b.id).\
                order_by('-data_dodania')[:1]
            p1 = Point(ost_lok[0].szer_geogr + ';' + ost_lok[0].dl_geogr)
            p2 = Point(l.szer_geogr + ';' + l.dl_geogr)
            l.km = int(distance.distance(p1, p2).km)
            # w wypadku gdy dodano banknot w tym samym miejscu co ostatnio
            if l.km == 0:
                dajax.add_data(GROWL_TXT['powt_miejsce'], 'display_jgrowl')
                return dajax.json()
        except:
            l.km = 0

        l.save()  # konieczne aby policzyć prędkość i deltę

        try:  # próbujemy policzyć prędkość i deltę czasu
            l.delta_punkt = delta_to_hours(l.delta)
            l.predkosc = l.km / l.delta_punkt
            l.save()
        except:
            pass  # nic się nie zmieniło
        lista = Lokalizacja.objects.filter(banknot__exact=b.id).\
            order_by('-data_dodania')
        data = serializers.serialize('json',
                                     lista)
        dajax.add_data(data, 'rysuj_trase')
        tab = table_html_output(lista, 'trasa',
                                ukryj_numer_seryjny(b.numer_seryjny),
                                b.nominal_banknotu)
        dajax.add_data(tab, 'fill_table')
        dl_szer = l.szer_geogr + ',' + l.dl_geogr
        for i in ciacho['ost']:
            if dl_szer in i:
                ciacho_ok = False
        if ciacho_ok:
            if len(ciacho['ost']) == 5:
                ciacho['ost'].pop(0)
            ciacho['ost'].append((dl_szer, l.wspolrzedne))
        dajax.add_data(json.dumps(ciacho), 'save_cookie')
    else:
        dajax.remove_css_class('#a-form input', 'error')
        for error in form.errors:
            dajax.add_css_class('#a-form #id_%s' % error, 'error')
    return dajax.json()
