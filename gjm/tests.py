#-*- coding: utf-8 -*-
import unittest
from gdzie_byl_kaziu.gjm.models import Banknot, Lokalizacja


class BanknotTestCase(unittest.TestCase):
    def setUp(self):
        self.banknot = Banknot.objects.create(numer_seryjny='AS1234567',
                                              nominal_banknotu='10')

    def test_create(self):
        self.assertTrue(self.banknot)

    def test_values(self):
        self.assertEqual(self.banknot.numer_seryjny, 'AS1234567')
        self.assertEqual(self.banknot.nominal_banknotu, '10')

    def suite():
        suite = unittest.TestSuite()
        suite.addTest(BanknotTestCase('test_create'))
        suite.addTest(BanknotTestCase('test_values'))
        return suite


class LokalizacjaTestCase(unittest.TestCase):
    def setUp(self):
        self.banknot = Banknot.objects.create(numer_seryjny='AS1234567',
                                              nominal_banknotu='10')
        self.lokalizacja = \
            Lokalizacja.objects.create(banknot=self.banknot,
                                       dl_geogr='19.00',
                                       szer_geogr='51.00')

    def test_create(self):
        self.assertTrue(self.lokalizacja)

    def test_get_coord(self):
        self.assertEqual(self.lokalizacja.wspolrzedne,
                         u'51° 0\' 0\'\' N, 19° 0\' 0\'\' E')

    def suite():
        suite = unittest.TestSuite()
        suite.addTest(LokalizacjaTestCase('test_create'))
        suite.addTest(LokalizacjaTestCase('test_get_coord'))
        return suite
